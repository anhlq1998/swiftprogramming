//
//  HeaderExpanded.swift
//  TaskSwiftUsuallyUse
//
//  Created by Le Quang Anh on 12/11/20.
//

import UIKit

protocol HeaderExpandedProtocol {
    func toogleHeader(section:Int)
}

class HeaderExpanded: UIView {
    var section:Int = 0
    var delegate:HeaderExpandedProtocol?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    func commonInit(){
        guard let view = UINib(nibName: "HeaderExpanded", bundle: nil).instantiate(withOwner: self, options: nil).first as? UIView else {return}
        view.frame = bounds
        self.addSubview(view)
        view.backgroundColor = .red
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleToogle)))
        
    }
    
    @objc func handleToogle(){
        delegate?.toogleHeader(section: section)
        print("aaa")
    }
}
