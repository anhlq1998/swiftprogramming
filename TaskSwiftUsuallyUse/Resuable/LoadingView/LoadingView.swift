//
//  LoadingView.swift
//  TaskSwiftUsuallyUse
//
//  Created by Le Quang Anh on 12/11/20.
//

import Foundation
import UIKit
class LoadingView : UIView {
    
    let effectView = UIView()
    let indiCator = UIActivityIndicatorView()
    let lbLoading = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    override func layoutSubviews() {
        effectView.layer.cornerRadius = 15
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    func commonInit(){
        // setup layout
        self.addSubview(effectView)
        effectView.translatesAutoresizingMaskIntoConstraints = false
        effectView.backgroundColor = #colorLiteral(red: 0.5993625522, green: 0.5987545848, blue: 0.6108174324, alpha: 1)
//        effectView.alpha = 0.2
//        effectView.fadeIn()
        effectView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        effectView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        
        effectView.addSubview(indiCator)
        effectView.addSubview(lbLoading)
        //-------------
        
        indiCator.translatesAutoresizingMaskIntoConstraints = false
        indiCator.color = .black
        indiCator.leftAnchor.constraint(equalTo: effectView.leftAnchor, constant: 15).isActive = true
        indiCator.heightAnchor.constraint(equalTo: self.heightAnchor,constant: 30).isActive = true
        indiCator.widthAnchor.constraint(equalTo: self.widthAnchor,constant: 30).isActive = true
        indiCator.topAnchor.constraint(equalTo: effectView.topAnchor, constant: 5).isActive = true
        indiCator.bottomAnchor.constraint(equalTo: effectView.bottomAnchor, constant: -5).isActive = true
        indiCator.startAnimating()
        
        //-------------
        
        lbLoading.translatesAutoresizingMaskIntoConstraints = false
        lbLoading.text = "Loading..."
        lbLoading.leftAnchor.constraint(equalTo: indiCator.rightAnchor,constant: 5).isActive = true
        lbLoading.rightAnchor.constraint(equalTo: effectView.rightAnchor,constant: -15).isActive = true
        lbLoading.centerYAnchor.constraint(equalTo: indiCator.centerYAnchor).isActive = true

        

        
    }
    
    
    
}

extension UIView{
    func fadeIn(){
        UIView.animate(withDuration: 0.8) {
            self.alpha += 10
        }
    }
    
    func fadeOut(){
        UIView.animate(withDuration: 0.4) {
            self.alpha -= 10
        }
    }
}
