//
//  ExpandedSwiftCell.swift
//  TaskSwiftUsuallyUse
//
//  Created by Le Quang Anh on 12/11/20.
//

import UIKit

class ExpandedSwiftCell: UITableViewCell {
    @IBOutlet var lbTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ExpandedSwiftCell {
    static var cell : String {
        get {
            return "ExpandedSwiftCell"
        }
    }
}
