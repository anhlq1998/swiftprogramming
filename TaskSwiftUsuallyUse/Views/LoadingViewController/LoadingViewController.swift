//
//  LoadingViewController.swift
//  TaskSwiftUsuallyUse
//
//  Created by Le Quang Anh on 12/11/20.
//

import UIKit

class LoadingViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var checK:Bool = false
    var arr : [Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createEvent()
        self.fecthApi()
        
    }
    func createEvent(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    func fecthApi(){
        self.showLoading()
        DispatchQueue.main.asyncAfter(deadline: .now() + Double(3)) {
            for i in 1...30{
                self.arr.append(i)
            }
            self.tableView.reloadData()
            self.hideLoading()
        }
    }
    
}

extension LoadingViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "asasda"
        return cell
    }
}
