//
//  ExpandedSwiftViewController.swift
//  TaskSwiftUsuallyUse
//
//  Created by Le Quang Anh on 12/11/20.
//

import UIKit

class ExpandedSwiftViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var arr : [ExpandedSwiftModel] = ListItem.getListExpanded()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createEvent()
        self.title = "Expand Cell"

    }
    func createEvent(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.separatorStyle = .none
        self.tableView.register(UINib(nibName: ExpandedSwiftCell.cell, bundle: nil), forCellReuseIdentifier: ExpandedSwiftCell.cell)
        
    }
}

extension ExpandedSwiftViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arr[section].isExpand ? arr[section].des.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExpandedSwiftCell.cell, for: indexPath) as? ExpandedSwiftCell
        cell?.lbTitle.text = arr[indexPath.section].des[indexPath.row].des
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = HeaderExpanded()
        view.section = section
        view.delegate = self
        return view
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    
}
extension ExpandedSwiftViewController: HeaderExpandedProtocol {
    func toogleHeader(section: Int) {
        let expand = !arr[section].isExpand
        self.arr[section].isExpand = expand
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    
    
    
}

