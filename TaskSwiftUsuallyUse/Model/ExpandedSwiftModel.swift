//
//  ExpandedSwiftModel.swift
//  TaskSwiftUsuallyUse
//
//  Created by Le Quang Anh on 12/11/20.
//

import Foundation
struct ExpandedSwiftModel {
    var title:String
    var des:[ExpandedSwiftDescription]
    var isExpand:Bool = false
    
}
struct ExpandedSwiftDescription{
    var des:String
}


