//
//  ExtensionUIViewController.swift
//  TaskSwiftUsuallyUse
//
//  Created by Le Quang Anh on 12/11/20.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showListFunction(){
        let alert = UIAlertController(title: "ListFunction", message: nil, preferredStyle: .actionSheet)
        let alertActionExpanded = UIAlertAction(title: "ExpandedTableView", style: .default) { (_) in
            let vc = ExpandedSwiftViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let alerActionLoading = UIAlertAction(title: "Loading", style: .default) { (_) in
            let vc = LoadingViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.addAction(alertActionExpanded)
        alert.addAction(alerActionLoading)
        self.present(alert, animated: true, completion: nil)
        
    }
     func showLoading(){
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {return}
        
        if ((window.viewWithTag(88888888)) != nil){
            return
            
        }
        let loading = LoadingView()
        loading.center = window.center
        loading.tag = 88888888
        window.addSubview(loading)
    }
    
     func hideLoading(){
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {return}
    
        window.viewWithTag(88888888)?.removeFromSuperview()
    }
}


